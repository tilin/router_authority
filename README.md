## router_authority
前端路由权限，根据用户不同的权限显示不同的菜单以及页面。 Vue2.x+Vuex+Element-ui+Axios+Mockjs。

## 项目预览
[点击预览](https://tilin.gitee.io/router_authority/#/login)
## 图片预览
![输入图片说明](https://images.gitee.com/uploads/images/2021/0225/194328_224b9fb9_2084888.png "2021-02-25_013713.png")

## 安装依赖、运行
```
npm install

npm run serve
```
## 注意
1. 此项目采用的路由方式为默认的hash，若采用history需要与后台配合好，否则页面刷新后显示错误；详见[Vue官方文档：HTML5 History 模式](https://router.vuejs.org/zh/guide/essentials/history-mode.html#%E5%90%8E%E7%AB%AF%E9%85%8D%E7%BD%AE%E4%BE%8B%E5%AD%90)
2. 在引入vue、element-ui的cdn时在开发环境报错但在生产环境没问题情况，此处采用先在vue.config.js中储存一个项目环境状态值，再在index.html中进行判断环境引入cdn；
3. 引入mockjs的cdn后进行打包，在本地开启的Live Server是没问题的，但在gitee开启pages出现mockjs不能用的问题，所以只好引入本地mockjs进行数据模拟。
