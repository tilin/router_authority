import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入axios
import axios from './utils/axios_defaults'
Vue.prototype.$http=axios
// Mockjs-数据模拟
import './mock/menusMock'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')