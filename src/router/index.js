import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

// 引入公共路由
import {commonRoutes} from '../utils/commonRoutes'

const router = new VueRouter({
  routes:commonRoutes
})
// 导航守卫
router.beforeEach((to, from, next) => {
  const userInfo=JSON.parse(sessionStorage.getItem('userInfo'))
  if(userInfo && userInfo.token){ //有token
      next()
  }
  else if(to.path!=='/login'){// 无token，跳转到登录页面
      next('/login')
  }
  else{ //登录页
      //更改article菜单栏状态-隐藏
      store.commit('changeToLogin',false)
      next()
  }
})
export default router
