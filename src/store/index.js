import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
// 引入拆分的state、mutations
import state from './state.js'
import mutations from './mutations.js'
// 开发环境 直接使用state的变量赋值会报错，但会修改成功
const isDev=process.env.NODE_ENV==='development'

export default new Vuex.Store({
    strict:isDev,
    state,
    mutations
})