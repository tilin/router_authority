import Mock from 'mockjs'

function userLogin(parmas){
    const parmasObj=JSON.parse(parmas.body)
    const sendObj={
        code:0,
        message:'获取数据成功',
        result:[
            {
                path:'/',
                name:'首页',
                component:'home',
                children:[
                    {path:'/home/child',name:'子页面',component:'child'},
                    {path:'/home/list',name:'列表页',component:'list'},
                ]
            }
        ]
    }
    if(parmasObj.token=='admin'){
        sendObj.result.push(
            {
                path:'/slider',
                name:'slider滑块',
                component:'slider'
            }
        )
    }else if(parmasObj.token=='test'){
        sendObj.result.push(
            {
                path:'/test',
                name:'测试页',
                component:'test'
            }
        )
    }else{
        sendObj={
            code:1,
            message:'没有该用户！'
        }
    }
    return sendObj
}
// 若设置axios的baseURL，使用mockjs拦截时必须是完整的url
Mock.mock('http://localhost:3000/getMenus','post',userLogin)
Mock.mock(/.*?\/login/,'get',{
    data:{code:0,message:'ok!'}
})
