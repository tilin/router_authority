// 默认公共路由配置
export const commonRoutes=[
    {path:'/',component:()=>import('../views/Home')},
    {path:'/home',redirect:'/'},
    {path:'/login',component:()=>import('comps/login.vue')}
]
export default {commonRoutes}