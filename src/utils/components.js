// 真实路由组件
export const routeMap={
    home:(resolve )=>require(['../views/Home.vue'],resolve),
    slider:(resolve )=>require(['comps/slider.vue'],resolve),
    err:(resolve )=>require(['comps/404.vue'],resolve),
    child:(resolve )=>require(['comps/child.vue'],resolve),
    list:(resolve )=>require(['comps/list.vue'],resolve),
    test:(resolve )=>require(['comps/test.vue'],resolve),
    // home:()=>import('../views/Home.vue'),
    // slider:()=>import('comps/slider.vue'),
    // err:()=>import('comps/404.vue'),
    // child:()=>import('comps/child.vue'),
    // list:()=>import('comps/list.vue'),
    // test:()=>import('comps/test.vue'),
}
/*如果用import引入的话，当项目打包时路由里的所有component都会打包在一个js中，造成进入首页时，需要加载的内容过多，时间相对比较长。
当你用require这种方式引入的时候，会将你的component分别打包成不同的js，加载的时候也是按需加载，只用访问这个路由网址时才会加载这个js。
你可以打包的时候看看目录结构就明白了。  */