import axios from 'axios'
// 使用网页进度条
const host='http://localhost:3000'
// const host='http://localhost:8080'
// 获取token
const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
let token=''
if(userInfo){
    token=userInfo.token
}
// 设置默认
const instance=axios.create({
    baseURL:host,
    timeout:5000,
    headers:{
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':token,
    }
})
// 请求拦截
instance.interceptors.request.use(config=>{
    console.log('请求拦截成功！')
    return config
},error=>{
    return error
})
// 响应拦截
instance.interceptors.response.use(response=>{
    console.log('响应拦截成功！')
    return response
},error=>{
    return error
})
export default instance