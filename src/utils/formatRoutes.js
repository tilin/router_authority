// 根据后端返回的数据进行改造，替换为真实路由
import {routeMap} from './components'
export const formatRoutes=(routes)=>{
    return routes.length&&routes.map(route=>{
        // 第一次进来，进行数据实例化，主要替换component的数据为真实组件
        const newRoutes={
            path:route.path,
            name:route.name,
            component:routeMap[route.component]
        }
        //是否存在子路由
        if(route.children){
            newRoutes.children=formatRoutes(route.children)
        }
        return newRoutes
    })
    
}
export default {formatRoutes}