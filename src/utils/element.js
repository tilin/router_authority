// elementui按需引入
import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css'
import {Card,Button,Message,Slider,Avatar,Form,FormItem,Input} from 'element-ui'
Vue.use(Card)
Vue.use(Button)
Vue.use(Slider)
Vue.use(Avatar)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
// Message挂载到vue实例上
Vue.prototype.$message=Message