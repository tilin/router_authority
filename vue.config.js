module.exports={
    // 基本路径
    publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
    // webpack内部配置
    configureWebpack:{
        resolve:{
            alias:{
                'comps':'@/components'
            }
        }
    },
    chainWebpack:config=>{
        const projectEnv=process.env.NODE_ENV
        // 开发阶段
        config.when(process.env.NODE_ENV==='development',config=>{
            config.entry('app').clear().add('./src/main.js')
        })
        // 生产阶段
        config.when(process.env.NODE_ENV==='production',config=>{
            config.entry('app').clear().add('./src/main-production.js')
            config.set('externals',{
                vue:'Vue',
                // mockjs:'Mock'
            })
        })
        // 保存项目环境，在index.html进行判断加载cdn
        config.plugin('html').tap(args=>{
            args[0].projectEnv=projectEnv
            return args
        })
    }
}